package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {
        int numofNeighbors = countNeighbors(row, col, CellState.ALIVE);
        CellState state = currentGeneration.get(row, col);

        if (state == CellState.ALIVE) {
            return CellState.DYING;
        } else if (state == CellState.DYING) {
            return CellState.DEAD;
        } else if (state == CellState.DEAD && numofNeighbors == 2) {
            return CellState.ALIVE;
        }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();

        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    private int countNeighbors(int row, int col, CellState state) {
        // add dir to (row, col) to get to neighbor (row, col)
        int numofNeighbors = 0;
        int dir[][] = { { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 },
                { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 } };
        for (int i = 0; i < dir.length; i++) {
            int newRow = row + dir[i][0];
            int newCol = col + dir[i][1];
            if (isValidCoordinate(newRow, newCol)) {
                if (currentGeneration.get(newRow, newCol) == CellState.ALIVE) {
                    numofNeighbors += 1;
                }
            }
        }
        return numofNeighbors;
    }

    public boolean isValidCoordinate(int row, int column) {
        int rows = currentGeneration.numRows() - 1;
        int columns = currentGeneration.numColumns() - 1;
        if (row < 0 || column < 0 || row > rows || column > columns) {
            return false;
        }
        return true;
    }

}
